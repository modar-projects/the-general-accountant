<p align="center">
  <a href="https://gitlab.com/modar-projects/the-general-accountant" title="The General Accountant">
    <img src="/Images/Logo/withTextWithEnhancement/onlinelogomaker-041821-0315-3596_auto_x2.png "width="80%" alt="The General Accountant"/>
  </a>
</p>


## :point_right: Description

### The    General    Accountant    is  a   web  application  that  functions  as  an accounting  system   with   capability to  

###  be  customized for  each  foundation and  provides management  for financial accounts and  automating for   

 ### essential accounting operations. ### 


<h2 align="center">🌐 Links 🌐</h2>
<p align="center">
    <a href="https://gitlab.com/modar-projects/the-general-accountant" title="The General Accountant">📂 Repo</a>
    ·
   
</p>


## 🚀 Features

- **Register System**

- **Forming chart of accounts**

- **Register financial operations as journal entries**

- **Posting to the ledger**

- **Accounts Balancing**

- **Preparation the trial balance**

-  **Preparation the final accounts**


## 📂 Screen Shots
 #### 1. Home : It's just a greeting page and define the services we provide in our system.

<p align="center">
  <a href="/Images/screenShots/1-home.png" title="screenShot-home">
    <img src="/Images/screenShots/1-home.png "width="80%" alt="screenShot-home"/>
  </a>
</p>

----
 #### 2. Signup : Here where user can register a new account in The General Accountant.

<p align="center">
  <a href="/Images/screenShots/2-signUp.png" title="screenShot-signup">
    <img src="/Images/screenShots/2-signUp.png "width="80%" alt="screenShot-signup"/>
  </a>
</p>

----
 #### 3. Register a New Business : Here where user can register a new business's details after register a new account.

<p align="center">
  <a href="/Images/screenShots/3-onBoarding.png" title="screenShot-onboarding">
    <img src="/Images/screenShots/3-onBoarding.png "width="80%" alt="screenShot-onboarding"/>
  </a>
</p>

----
 #### 4. Signin : Here where user can sign in into The General Accountant.

<p align="center">
  <a href="/Images/screenShots/4-signIn.png" title="screenShot-signin">
    <img src="/Images/screenShots/4-signIn.png "width="80%" alt="screenShot-signin"/>
  </a>
</p>

----
 #### 5. Chart of Accounts : Here where user can manage its own financial accounts and browse them.

<p align="center">
  <a href="/Images/screenShots/5-Chart-of-accounts.png" title="screenShot-chartOfAccounts">
    <img src="/Images/screenShots/5-Chart-of-accounts.png "width="80%" alt="screenShot-chartOfAccounts"/>
  </a>
</p>

----
 #### 6. Add an Account : Here where user can add a new financial account.

<p align="center">
  <a href="/Images/screenShots/6-Add-account.png" title="screenShot-addAccount">
    <img src="/Images/screenShots/6-Add-account.png "width="80%" alt="screenShot-addAccount"/>
  </a>
</p>

----
 #### 7. Edit an Account : Here where user can edit existing financial account.

<p align="center">
  <a href="/Images/screenShots/7-edit-account.png" title="screenShot-editAccount">
    <img src="/Images/screenShots/7-edit-account.png "width="80%" alt="screenShot-editAccount"/>
  </a>
</p>

----
 #### 8. Entry Document : Here where user can write new entries  then  add a new entry document and here user can edit an
 ### existing entry document ,delete and navigate to it. 

<p align="center">
  <a href="/Images/screenShots/8-Entry-doc.png" title="screenShot-entryDoc">
    <img src="/Images/screenShots/8-Entry-doc.png "width="80%" alt="screenShot-entryDoc"/>
  </a>
</p>

----
 #### 9. Journal : Here where user can see the journal of his business and export it as CSV.

<p align="center">
  <a href="/Images/screenShots/11-journal.png" title="screenShot-journal">
    <img src="/Images/screenShots/11-journal.png "width="80%" alt="screenShot-journal"/>
  </a>
</p>

----
 #### 10. Ledger : Here  where  user can search about specific existing financial account and see related ledger page  and 
 ### export it as CSV.

<p align="center">
  <a href="/Images/screenShots/12-ledger.png" title="screenShot-ledger">
    <img src="/Images/screenShots/12-ledger.png "width="80%" alt="screenShot-ledger"/>
  </a>
</p>

----
 #### 11. Trial Balance : Here where user can search about specific existing financial account and see related trial balance 
 ### and export it as CSV.

<p align="center">
  <a href="/Images/screenShots/13-Trial-balance.png" title="screenShot-trialBalance">
    <img src="/Images/screenShots/13-Trial-balance.png "width="80%" alt="screenShot-trialBalance"/>
  </a>
</p>

----
 #### 12. The Final Accounts : Here  where  user  can  search  about  just Trading account and see related data and export it 
 #### as CSV.

<p align="center">
  <a href="/Images/screenShots/14-Final-accounts.png" title="screenShot-finalAccounts">
    <img src="/Images/screenShots/14-Final-accounts.png "width="80%" alt="screenShot-finalAccounts"/>
  </a>
</p>


## 🦋 Prerequisite

- [MySQL](https://www.mysql.com/ "MySQL")  Installed

- [PHP +7.3](https://www.php.net/ "PHP")  Installed

- [Node.js +12.18](https://nodejs.org/en/ "NodeJS")  Installed

## 🛠️ Installation Steps

1. Clone the repository

```Bash
git clone https://gitlab.com/modar-projects/the-general-accountant.git
```

2. Create new database named as 'general_accountant'

3. Import 'general_accountant.sql' file into created database

4. Change the working directory

```Bash
cd Back-end
```

5. Run the project 

```Bash
php artisan serve
```

6. Change the working directory

```Bash
cd Front-end
```

7. Run the project 

```Bash
npm start
```

**🎇 You are Ready to Go!**

## 👷 Built with

 [MySQL](https://www.mysql.com/ "MySQL"): as Database Engine

 [Laravel](https://laravel.com/ "Laravel"): as RESTful API

 [React](https://reactjs.org/ "React"): as Front End


## 🎊 Further Approach

- [ ] Add capability to create bills.

- [ ] Develop mobile version.

- [ ] Add feedback.

## 🧑🏻 Author

**Modar Alkasem**

- 🌌 [Profile](https://gitlab.com/modarAlkasem/ "Modar Alkasem")

- 🏮 [Email](mailto:modarAlkasem@gmail.com?subject=Hi%20from%20the-general-accountant "modarAlkasem@gmail.com")



